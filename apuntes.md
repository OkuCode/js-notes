# Programación III - Sesión 01 -  05/02/2022

## Historia de javascript

Creador del lenguaje *Brendan Eich* en 1995, lenguaje dinamico, que interactua con HTML y CSS que són los estandares de la web, que ahora se convierte en el más usado por todas las compañias de todo el mundo, se puede crear desde paginas dinamicas hasta sistemas embebidos, sistemas operativos, api, servicios de backend, entre otros. 

## Donde ejecutar javascript

JS al ser un lenguaje que trabaja en la web lo podemos utilizar en cualquier navegador, usando la consola de javascript, en navegadores basados en chromium se puede usar la consola usando la tecla _f12_, lo mismo que en firefox.

## Ejecutar Javascript

Para ejecutar javascript usaremos la siguiente sentencia de codigo en la consola

- `console.log("Hello, from javascript");`
- `document.write("Hello, from javascript");`

## Otras formas de ejecutar javascript

- Se necesita crear un archivo html, y usar la etiqueta `<script>console.log("Hello");</script>`
- O especificar la ruta del archivo de javascript en la etiqueta _src_
	- `<script src="filename.js></script>"`, y escribir el codigo que queramos, en este caso en el archivo _filename.js_.

## Declaración de variables

Se pueden utilizar las siguientes formas para declarar variables usando los siguientes palabras reservada o *Keywords*

- `var`: utilizado desde los inicios de JS, pero esta declaración de variables tenia problemas con el programa a la hora de ejecución ya que permiten llamar variables de forma global y podia tomar variables que estaban en otro lado del programa
- `let`: Introducido en ES6 y que corrige los errores de var al ser declarado una sola vez y tener su propio scope
- `const`: Otra forma de declarar variables, pero estos valores no cambiaran ya que const viene de la palabra inglesa _constant_ que signigica constante y tiene el mismo comportamiento que tiene let en el scope

### Ejemplos

- Declaración con var: `var nombre = "Juan";`
- Declaración con let: `let apellido = "Barco";`
- Declaracion con const: `const CEDULA = 1053838859;` -> Las contantes generalmente usan mayusculas para definir que no cambiaran en el tiempo

## Primitive Types - tipos primitivos

Los tipos primitivos en programación son tipos de datos que se manejan comúnmente como numeros, flotantes, string o caracteres, y booleanos

- integers: `let edad = 25;`
- floats: `let porcentaje = 10.2;`
- strings: `let nombreCompleto = "Juan David Rodas Barco";`
- booleans: `let on = true`

- _Tip: usando el metodo `typeof()` puedes verificar el tipo de dato al que pertenece una varible_

## Compound types - tipos compuestos

Són estructuras que almacenan todo tipos de datos.

- Array = `let shoppingList = ["Arroz", 10, true, 13.5]`
- Objects = `let person = {name: "Juan David Rodas Barco", age: 27, isAlive: true}`

# Programación III - Sesión 02 -  10/02/2022

## Formas de iterar valores - Ciclos

Los ciclos sirven para iterar una coleccion de valores, o un rango de numeros para hacerlo mucho mas rapido

### For

Mientras tenga un rango se recorrera el ciclo, cuando llega saldra del ciclo

- Ejemplo de ciclo for: ```for (let i = 0; i<= 10; i++){ console.log(i)}```
- Ejemplo de ciclo object: `for(key in names){console.log(key)}` -> Este ciclo es usado para tipos compuestos como arreglos y objetos

### While

Mientras una condición sea verdadera seguira recorriendo el ciclo

- Ejemplo de un ciclo while: ``while(true){ console.log("Hola")}` -> Tener cuidado a la hora de usar este tipo de ciclos, ya que puedes tener un ciclo infinito por eso tener un acumulador para que cumpla la condicion o usar la palabra reservada `brake` para detenerlo

## Funciones

Son bloques de codigo que cumplen una acción espeficica, se pueden llamar en todo la ejecución del programa y es mas sencillo.

- Ejemplo:
	 ``` 
	// Nombre de la funcion con la palabra reservada function
	function greeting(){
		console.log("Hola desde javascript");
	};
		
	greeting(); // Llamada de la función
	```
- Ejemplo como expresion:
	 ```
	let plusTwoNumbers = function(n1, n2) {
		console.log(n1 + n2); 	
	};

	console.log(plusTwoNumbers(1, 2));
	```
- Ejemplo como _Arrow Function_:
	 ```
	const plusTwoNumbers = (n1, n2) => { return n1 + n2 };
	```

## Scope

Contexto actual de ejecucion, el momento en que son visibles o tiene significado las variables dentro de la ejecución del programa.

- Ejemplo: 
	 ```
	var x = 10; // Scope global: variable donde se puede acceder desde cualquier lado y existira en todo lado
	let y = 20; // Scope global: variable global pero se accedera donde se necesite y existira donde se llame

	function plusTwoNumbers(){
		let z = 30 ; // Scope de funcion: Esta variable solo se vera dentro de este bloque
		console.log(x + z); // Dara el resultado 40 ya que llamará a x que esta en el scope global
		console.log(y + z); // Data el resultado 50 ya que llamará a y que esta en el scope global
	}

	console.log(x + y); // Dara el resultado 30 ya que ambas variables se encuentran de manera global
	console.log(y + z); // Dara error ya que z esta en el Scope de funcion 
	console.log(plusTwoNumbers()); // Dará el valor 40 y 50 que se imprime al momento de llamar a la función
	```

## Coerción de datos

Es la forma como el lenguaje de programación convierte los tipo de dato

### Implicita 

El lenguaje intepretara las variables y tratara de convertirlos de manera como lo interpreta

### Explicita

Es la forma como le decimos el lenguaje como quiere que convertirmos los tipos de datos 


# Programación III - Sesión 03 - 19/02/2022

## Arreglos y Objetos

Estas sòn las estructuras de datos mas importantes en javascript ya que con ellas podemos acceder a un abanico de posibilidades, desde manejar tipos de datos de datos diferentes hasta crear funciones que utilicen estos datos para hacerlo operaciones de manera mucho màs sencilla.

- Declaraciòn de un arreglo en javascript: Usamos los corchetes y dentro especificamos los datos que queremos

	`let nombres = ["Remilia", "Alice", "Nue", "Ringo"];`

- Los arreglos no solo se limitan a tener un tipo de dato predeterminado, tambien pueden tener distintos tipos de dato 

	`let cosas = ["Hola", 10, true, ["Colombia", "Suiza"]];`

- Podemos acceder a ellos usando la notaciòn con corchetes y dando el numero de la posiciòn en la que queremos obtener el datos

	```
	let nombres = ["Remilia", "Alice", "Nue", "Ringo"];
	console.log(nombres[0]) // Remilia will print out to the console
	```

- Podemos cambiar los valores de nuestro arreglo haciendo uso de la notacion con corchetes y el valor en el cual queremos cambiarlo

	```
	let nombres = ["Remilia", "Alice", "Nue", "Ringo"];
	nombres[2] = "Reisen";
	console.log(nombres) 

	// ["Remilia", "Alice", "Reisen", "Ringo"] 

	it change the second position value Nue for Reisen
	```

- **Nota**: Recuerda que para indexar nuestros valores empezamos desde el valor 0

- Podemos iterar nuestros elementos de nuestro arreglo e imprimir los elementos

	```
	let nombres = ["Remilia", "Alice", "Nue", "Ringo"];
	for (let i = 0; i < nombres.length < i++){
		console.log(nombres[i]);
	};

	// Remilia
	// Alice
	// Nue
	// Ringo 

	it iterates and print the values to the console
	```
- Tenemos metodos para operar nuestros arreglos de los cuales los mas utilizadosson `pop(), push(), shift(), unshift()` entre otros para saber mas puedes ir a [este enlace](https://www.w3schools.com/js/js_array_methods.asp)

## Objetos

Los objetos son colecciones de datos que se componen de dos componentes _keys_ son un identificador unico con el que podemos verificar el dato, _value_ que es valor que contiene el key, de aqui se origino uno de los tipo de archivo mas utilizado en el desarrollo el Javascript Object Notation o JSON.

- Para definir un objeto hacemos uso de las llaves y definimos las keys y values de cada uno

	```
	let song = {
		artist: "System Of A Down",
		song: "Spiders",
		released: true,
		year: 1995,
		members: ["Serj Tankian", "Daron Malakian", "Shavo Odadjian, John Dolmayan"]
	}	
	
	This define a basic object, is also known with another names
	in other programming language like dictionaries in python or hashmap in rust
	```

- Los objetos pueden tener objetos anidados tambien

	```
	let songs = {
		artist: {
			name: System Of A Down",
			song: "Spiders",
			released: true,
			year: 1995,
			members: ["Serj Tankian", "Daron Malakian", "Shavo Odadjian", "John Dolmayan"]
		},

		artist: {
			name: "Metallica",
			song: "Fade to black",
			released: true,
			year: 1984,
			members: ["Cliff Burton", "James Hetfield", "Lars Ulrich", "Kirk Hammett"]
	   };
	}	

	Is also known that this type of data structure compose the JSON files	
	```
- Ademàs los objetos pueden tener funciones dentro de los mismos, puedes declarar arrow function tambien

	```
	let song = {
		artist: "System Of A Down",
		song: "Spiders",
		released: true,
		year: 1995,
		members: {
			vocalist: "Serj Tankian",
			guitar: "Daron Malakian",
			bassist: "Shavo Odadjian",
			drummer: "John Dolmayan"
		},
		choir: function() {
			return 'Dreams are made, winding, Through my head ';
		},

		beggining: () => {
			return 'The piercing radiant moon (The piercing radiant moon)\nThe storming of poor June (Storming of poor June)\nAll the life running through her hair (Through her hair)';
		}
	};		
	```
- Puedes acceder a los datos del objeto usando la notaciòn con punto o con corchetes, recomiendo la primera ya que es màs facil para poder leerlo

	```
	First: console.log(song.artist);
	Second: console.log(song["members"]);
	Thrid: console.log(song.beggining());

	First: System Of A Down

	Second: 
		members: {
			vocalist: "Serj Tankian",
			guitar: "Daron Malakian",
			bassist: "Shavo Odadjian",
			drummer: "John Dolmayan"
		}

	Thrid: "The piercing radiant moon (The piercing radiant moon)
			The storming of poor June (Storming of poor June)
			All the life running through her hair (Through her hair)"
	```
- Puedes cambiar o actualizar los valores usando la notacion con punto o con notacion con corchetes

	```
	song.artist = "SOAD";
	song["year"] = 1996;

	This update all the values selecting their respective key

	```

# Programación III - Sesión 04 ~ 05 -  26/02/2022 ~ 05/03/2022 

## Programación orientada a objetos

El paradiga de programaciòn orientado a objetos es una forma nos permiten modelar de manera màs especifica objetos de la vida real, esto se a estandarizado al redededor de todos los lenguajes de programaciòn, para construir de manera efectiva software que podamos usar en entornos reales

### Clase

Una clase es una plantilla base que nos sirve para construir modelos, que tiene caracteristicas comùnes o unicas y comportamientos especificos que nos ayudan a identificarlo de manera màs facil, el ejemplo màs claro puede ser un carro, sus caracteristicas basicas son: tiene 4 llantas, un color, y unas acciones especificas, como lo sòn tocar la bocina, acelerar, frenar entre otras

### Objeto

Es una instancia de una clase bajo estandares especializados, pero de manera màs general es como nace un objeto de la plantilla ya concebida, con sus caracteristicas unicas y sus funciones predefinidas

### Atributo

Caracteristicas comunes o unicas con las que cuentan nuestras clases y sirven para diferenciar un objeto de otro

### Metodo

Son funciones especificas que se encuentràn dentro de nuestra clase y que tomaran nuestros objetos para ser ejecutadas al momento de ser creadas

## Clases en javascript

Las clases en javascript se manejan diferente, pero comparten ciertas similitudes con otros lenguajes de programaciòn, por ejemplo en su definiciòn se debe hacer para la primera letra con mayuscula para definir la clase de manera mas especifica, estas clases incluyen un metodo constructor, por defecto de sebe necesita, ya que al instanciar javascript data una excepciòn al no incluir la palabra _new_, los metodos de estas clases se pueden definir mediante una funciòn normal o usando **Arrow Function** veremos algunos ejemplos

1. Ejemplo de clase en Python

	```
	class Rectangle:
		# Constructor method in python

		def __init__(self, width, height):
			self.nombre = nombre
			self.edad = edad
		
		def area(self):
			return f'Area: {abs((self.height * self.width))}'
		
		def perimeter(self):
			return f'Perimeter: {2.0 * abs((self.height * self.width))}'

		def __repr__(self):
			return f'Rectangle({self.width}, {self.height})'

		def __str__(self):
			return f'Rectangle has: Width {self.width} and Height {self.height}'
	```


2. Classes en javascript

	- Usamos de class keyword y agregamos un metodo llamado constructor, para declarar atributos privados usamos el # simbolo


	```
	class Rectangle {
		constructor(width, height) {
			this.height = height;
			this.width = width;
		}

		area() {
			return `Area: ${Math.abs((this.height * this.width))}`;
		}
		
		// Getters in javascript

		getArea() {
			return this.area();
		}

		perimeter() {
			return `Perimeter: ${2.0 * Math.abs((this.height - this.width))}`;
		}

		getPerimeter() {
			return this.perimeter();
		}
	}
	```

Aparte de eso tambien tenemos en javascript otra forma de crear clases llamada _metodos constructores_ son una forma mucho mas simple de crear nuestras clases para poder trabajarlas

- Ejemplo de function constructora en javascript

	```
	// Constructor function
	// Like classes but more simple

	function Point(x, y) {
		this.x = x;
		this.y = y;
		this.hpt = () => {
			return `Hypotenuse: ${Math.hypot(this.x, this.y)}`;
		};

		this.getHpt = () => {
			return this.hpt();
		}
	} 
	```

Como se puede apreciar es una forma mas simple de crear clases, pero dando mayor facilidad a la hora de implementar, ademas de eso no requiere de metodo constructor para crear la instancia ya que podemos hacerlo solo con la palabra reservada new

## Instanciar objetos en javascript

La manera de instanciar un objeto en javascript se hace con la palabra reservada `new`, a diferencia de python siempre a de necesitar la palabra reservada sino dara error a la hora de crearla

- Ejemplo de instancia a un objeto y llamada de metodos en python

	```
	# Object Instatiated
	rect = Rectangle(120, 50)

	# Accessing a method
	print(rect.area())
	print(rect.perimeter())
	print(rect.__str__())
	```
- Ejemplo de instancia a un objeto y llamada de metodos en javascript	

	```
	// Using classes
	// Instatiated the name of the class you need the reserved keyword new

	let rect = new Rectangle(120, 34);
	let rect_two = new Rectangle(45, 23);

	// Access a values
	console.log(rect.height);
	console.log(rect.width);

	// Using the methods
	console.log(rect.getArea());
	console.log(rect.getPerimeter());

	// Using function constructor

	let point = new Point(30, 40)

	// Accessing values

	console.log(point.x);
	console.log(point.y);

	// Using methods
	console.log(point.getHpt());
	```

# Programación III - Sesión 06 -  26/03/2022

## Colas, Pilas y Queue

Son estructuras de datos como las **Listas simplemente y doblemente enlazadas** que tienen la facultad de que debe atender la primera peticiòn y al final sacar el ultimo que esta al final de la cola

```	| Another element that wants to join |
	| End of a element to attend a petition |
| Waiting |
| Waiting |
| Waiting |
	| If is already attend and also the first element go out and leave the free space |
```
__Representaciòn de una cola__

- Ejemplo de cola en javascript
```
// Example of cola

function ColaFila() {
    this.element = {};
    this.end = 0;
    this.start = 0;

    this.encolar = (data) => {
        this.element[this.end] = data;
        this.end += 1;
    }

    this.desencolar = () => {
        if(this.end === this.start) {
            return null;
        } else {
            const INFO = this.element[this.start];

            delete this.element[this.start]
            this.start += 1;
            return INFO;
        }
    }

    this.lengthCola = () => {
        return this.end - this.start;
    }

    this.print = () => {
        if (this.lengthCola() === 0) {
            return null;
        } else {
            let answer = '';
            for(let i = this.start; i < this.end; i++){
                answer += `${this.element[this.start] }`; 
            }

            return answer;
        }
    }
}

const cola = new ColaFila();
cola.encolar("Reisen");
cola.encolar("Reimu");
cola.encolar("Remilia");
cola.encolar("Alice");
console.log(cola)
```
