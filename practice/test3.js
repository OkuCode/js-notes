// Here we implement the LinkedList Data Structure in JS

// Node class - the container of all kinds of data

class Node {
    constructor(data, next){
        this.data = data;
        this.next = next;
    };
}

// Linked list class we
class LinkedList {
    constructor() {
        this.head =  null;
        this.size =  0;
    };

    // Adding elements
    add(data){
        // Create a new node, but there's a way to created with a method
        const NODE = new Node(data, null);
        // Check if the head is empty
        if(this.head === null){
            this.head = NODE;
        } else {
            let current = this.head;

            while(current.next){
                current = current.next;
            }

            current.next = NODE;
        };

        this.size += 1;
    };

    // Add in a specific position
    addInIndex(index, data){
        if(index < 0 || index > this.size){
            return `Invalid index, Operation failed !!!`;
        } else {
            const NODE = new Node(data, null);
            let current = this.head;
            let previous;

            // Insert to the begin in other words the head
            if(index === 0){
                NODE.next = current;
                this.head = NODE;
            } else {
                for(let i = 0; i < index; i++){
                    previous = current;
                    current = current.next;
                };
                
                // Here asing the new Node to the current and previous to the new
                NODE.next = current;
                previous.next = NODE;
            };

            this.size += 1;
        };

    };

    // Delete node by index
    deleteByIndex(index){
        if(index < 0 || index > this.size){
            return `Invalid Index, Operation Failed !!!`;
        } else {
            let current = this.head;
            let previous;

            // It means the head
            if(index === 0){
                this.head = current.next;
            } else {
                for(let i = 0; i < index; i++){
                    previous = current;
                    current = current.next;
                };

                previous.next = current.next;
            };

            this.size -= 1;
            return current.data;
        };
    };

    // Print the elements
    print(){
        if(this.head === null){
            return `There's no data available`
        }             

        let current = this.head;
        let answer = "";

        // Here is the error because current.next always points to null
        while(current){
            if(typeof(current.data) == "string"){
                answer += current.data += "-> ";
            } else {
                answer += `${current.data} -> `;
            };

            current = current.next;
        };

        answer += "null"
        return answer;
    };
}

const linkedOne = new LinkedList();
linkedOne.add(23);
linkedOne.add("Remilia");
linkedOne.add({song: "Fade to black"});
linkedOne.add(["Rest", true, 1]);
linkedOne.print();