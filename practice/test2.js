let array = ["Hola", true, 1, 4, 5];

const lengthOfArray = () => {
    return array.length;
}

const sortArray = () => {
    return array.sort();
}

const printArray = () => {
    return array;
}

const deleteElement = (element) => {
    if (lengthOfArray() === 0) {
        return `There's no elements on the array`;
    } else {
        let dato;
        for (let i = 0; i < lengthOfArray(); i++) {
            // But doesn't work
            if(array[i] === element){
                dato = array.splice(array.indexOf(array[i]), 1, element);
            }
        }
        //// Verify the deleted element
        if(dato != undefined) {
            return `The element has been deleted: ${dato[0]}`
        } else {
            return `The element doesn't exist`
        }
    }
}

const deleteByIndex = (index) => {
    let data;
    if(lengthOfArray() === 0){
        return `There's no elements on the array`;
    } else {
        for(let i = 0; i < lengthOfArray(); i++){
            if(index === array.indexOf(array[i])){
                data = array.splice(i, 1);
            }
        }

        if(data != undefined){
            return `The element has been deleted: ${data[0]}`
        } else {
            return `The element doesn't exist: ${data}`
        }
    }
}

//let array = ["Hola", true, 1, 4, 5];
// [true, 1, 4, 5]
// [true, 1, 4]
// [true, 4]
// [true]

console.log(array)
console.log(deleteElement("Hola"))
console.log(array)
