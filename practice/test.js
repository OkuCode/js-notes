// Example of cola
function ColaFila() {
    this.element = {};
    this.end = 0;
    this.start = 0;

    this.encolar = (data) => {
        this.element[this.end] = data;
        this.end += 1;
    }

    this.desencolar = () => {
        if(this.end === this.start) {
            return null;
        } else {
            const INFO = this.element[this.start];

            delete this.element[this.start]
            this.start += 1;
            return INFO;
        }
    }

    this.lengthCola = () => {
        return this.end - this.start;
    }

    this.print = () => {
        if (this.lengthCola() === 0) {
            return null;
        } else {
            let answer = '';
            for(let i = this.start; i < this.end; i++){
                answer += `${this.element[this.start] }`; 
            }

            return answer;
        }
    }
}

const cola = new ColaFila();
cola.encolar("Reisen");
cola.encolar("Reimu");
cola.encolar("Remilia");
cola.encolar("Alice");
console.log(cola)
