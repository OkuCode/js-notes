# Yuzu Setup

Here we collect all the resources in order to work with yuzu emulator in this machine

- Fer's Workstation:
    - Processor: AMD FX 8320 - Compatible processor
    - RAM: 20 GB - DDR3
    - GPU: Nvidia GTX 1050 TI from EVGA - OpenGL 4.6 and Vulkan 1.3

## Resources

There's a subreddit for [Yuzu Piracy](https://www.reddit.com/r/NewYuzuPiracy/) so search for information

## Games

Download the games from here:

- [Games](https://www.ziperto.com/https://www.ziperto.com/https://www.ziperto.com/https://www.ziperto.com/)
- [Games #2](https://nxbrew.com/category/games/switch-nsp/)
- [Games #3](https://nsw2u.xyz/nintendo-switch-emulator-for-pc-yuzu)

## Keys and Firmware

In order to use yuzu, you need keys for them, *Then we need to download from another source*:

- [Option #1](https://github.com/CapitaineJSparrow/emusak-ui)
- [Guide for yuzu setup ~ With all the tools](https://github.com/Abd-007/Switch-Emulators-Guide/blob/main/Yuzu.md#section-2-installing-keys-and-firmware)
- [Guide by BsoD Gaming](https://www.youtube.com/watch?v=93xsKERji60)

