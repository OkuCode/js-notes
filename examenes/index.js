class Stack {
    constructor(elements, sortElements) {
        this.elements = elements || [];
        this.sortElements = sortElements || [];
    }

    insertToEnd = (item) => {
        if(item === undefined){
            return `Insert a valid value`;
        } else {
            return this.elements.push(item);
        }
    }

    insertToBegin = (item) => {
        if(item === undefined){
            return `Insert a valid value`;
        } else {
            return this.elements.unshift(item);
        }
    }

    lengthStack = () => {
        return this.elements.length;
    }

    removeElementAtLast = () => {
        const DATA = this.elements[this.elements - 1];
        this.elements.pop();
        return `The element has been deleted: ${DATA}`;
    }

    removeElementAtBegin = () => {
        const DATA = this.elements[this.elements - 1];
        this.elements.shift();
        return `The element has been deleted: ${DATA}`;
    }

    isEmpty = () => {
        return (this.lengthStack() === 0) ? true: false;
    }

    printStack = () => {
        return this.elements;
    }

    sortStack = () => {
        return this.elements.sort()
    }

    // Valide if the user has a preferential role
    hasPreferentialRole = () => {
        // Check the role and sorted by role
        for(let i = 0; i < this.lengthStack(); i++){
            if(this.elements[i].preferential === true){
                this.sortElements.unshift(this.elements[i]);
            } else {
                this.sortElements.push(this.elements[i])
            }
        }

        // Print the elementes sorted by role
        return this.sortElements.sort((a, b) => a.name[1] - b.preferential[1])
    }
}

// Creating the new stack 
const STACK = new Stack();

// Appending elements
STACK.insertToEnd({name: "Laura", age: 26, preferential: false});
STACK.insertToEnd({name: "Daniela", age: 19, preferential: false});
STACK.insertToEnd({name: "Armando", age: 45, preferential: true});
STACK.insertToEnd({name: "Luis Miguel", age: 54, preferential: false});
STACK.insertToEnd({name: "Viviana", age: 36, preferential: false});
STACK.insertToEnd({name: "Jaime", age: 14, preferential: true});
STACK.insertToEnd({name: "Lila", age: 76, preferential: true});
console.log(STACK.hasPreferentialRole());

