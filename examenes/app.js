// A: 1,2,3,4,5,6,7,8,9,10,12,14

class Company{
    constructor(nombre, edades, nombre_empleados, nit, tipo, empleados, generos, cabeza_hogar, nomina){
        // Definiendo los tipo de dato
        
        this.nombre = nombre || [];
        this.edades = edades || [];
        this.nombre_empleados = nombre_empleados || [];
        this.nit = nit || 0;
        this.tipo = tipo;
        this.empleados = empleados || [];
        this.generos = generos || ["F", "M", "O"];
        this.cabeza_hogar = cabeza_hogar || [true, false, undefined];
        this.nomina = nomina;
    };

    datosIniciales(){
        // Para pedir los datos de la empresa si no cumplen con las siguientes condiciones propuestas 10
        let intentos = 0;
    
        do {
            this.nombre = prompt("Por favor ingrese el nombre de la empresa");
            this.nit = prompt("Por ingrese el nit de la empresa");

            if(this.nombre.length === 0 && this.nit.length === 0){
                alert("Ingrese los datos correspondientes de la empresa");
            } else if (this.nit.length <= 10){
                alert(`El nit debe superar los 10 caracteres \nTotal de caracteres: ${this.nit.length}`);
            } else {
                alert(`Datos creados satisfactoriamente: ${this.nombre}, ${this.nit}`);
            }

            intentos += 1;
            
        } while(intentos < 1);

        return alert("Datos creados");
        
    }
    
    ingresarEmpleado(){
        let numeroRegistros = 0;
        
        do {
            // Verificar que los datos del empleado no sean vacios
            this.nombreEmpleado = prompt("Ingresa el nombre del empleado");

            if(this.nombreEmpleado.length === 0){
                alert("El nombre del empleado no puede estar vacio");
            } else {
                this.nombre_empleados.push(this.nombreEmpleado)
                alert("El nombre del empleado se a guardado");
            }

            // Verificar que la edad del empleado no sea cero y que sea mayor de edad
            this.edad = parseInt(prompt("Ingrese la edad del empleado"));
            (this.edad <= 0 || this.edad <= 18 || this.edad.length === 0) ? 
                alert("El empleado debe tener una edad valida y ser mayor de edad") : this.edades.push(this.edad);
        
        
            // Verificar que el genero del empleado corresponda al que esta asignado
            this.genero = prompt("Ingrese el genero del empleado");
            for(let i = 0; i < this.generos.length; i++){
                if(this.genero.toUpperCase() === this.generos[i]){
                    alert(`El genero es ${this.generos[i]}`);
                    break;
                } else {
                    alert(`Genero no corresponde al ingresado: ${this.genero} no es ${this.generos[i]}`);
                }
            }
        
            // Saber si es cabeza de hogar o no
            this.esCabezaDeHogar = prompt("Ingrese si es cabeza de hogar (Responde Si o No)")
            // Valido si es cabeza de hogar con si y no, le asigno un valor booleano segun la respuesta

            if(this.esCabezaDeHogar.toLowerCase() === "si"){
                this.esCabezaDeHogar = true;
            } else if (this.esCabezaDeHogar.toLowerCase() === "no"){
                this.esCabezaDeHogar = false;
            } else {
                alert("Valor invalido")
            }

        
            for(let i = 0; i < this.cabeza_hogar.length; i++){
                if(this.esCabezaDeHogar === this.cabeza_hogar[i]){
                    alert(`Es cabeza de hogar: ${this.esCabezaDeHogar}`);
                    break;
                } else {
                    alert(`No es cabeza de hogar: ${this.esCabezaDeHogar}`);
                    break;
                }
            }

            numeroRegistros += 1;
            
        } while(numeroRegistros < 2)

        return alert("Empleados registrados correctamente");
            
    }

    hombres(){
       return this.nombre_empleados
    }

    total_nomina(){}

}

Company.prototype.toString = function companyRepr() {
    return `Book { \n\tnombre: ${this.nombre}, \n\tedades: ${this.edades}, \n\tnombre empleados: ${this.nombre_empleados}, \n\tnit: ${this.nit} \n\ttipo: ${this.tipo}, \n\templeados: ${this.generos}, \n\tcabeza: ${this.cabeza_hogar}, \n\tnomina: ${this.nomina} }`;
}

const company = new Company();
company.datosIniciales();
company.ingresarEmpleado();
console.log(company.toString());



