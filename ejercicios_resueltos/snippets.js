// Ejercicio 1
let nameUser = prompt("Hello, type your name: ");
alert(`Now ${nameUser}, you are on Matrix`);

// Ejercicio 2

let integer = prompt("Hello, please type an integer: ");
let float = prompt("Now type an float number: ")

const plusIntFloat = (int, float) => {
    let total = parseInt(int) + parseFloat(float)
    return total;
};

alert(`Result: ${plusIntFloat(integer, float)}`);

// Ejercicio 3

let firstNum = prompt("Hello, please type a number: ");
let secondNum = prompt("Now type another number: ")

let total = parseInt(firstNum) + parseInt(secondNum);
alert(`Result: ${plusIntFloat(firstNum, secondNum)}`);

let thirdNumber = prompt("To finish type other number: ");
let finalResult = parseInt(thirdNumber) * total;

alert(`Final result: ${finalResult}`);

// Ejercicio 4

let kmRecorridos = prompt("Ingrese la cantidad de kilometros recorridos: ");
let gasolinaConsumida = prompt("Ingrese la cantidad de combustible gastado: ");

const consumoPorKm = (km, gc) => {
    let consumo = parseFloat(km) / parseFloat(gc);
    return consumo;
};

alert(`El consumo de combustible por kilometro es de: ${consumoPorKm(kmRecorridos, gasolinaConsumida)}`);

// Ejercicio 6

let numOne = prompt("Please type a first number: ");
let numTwo = prompt("Please type a second number: ");
let numThree = prompt("Please type a thrid number: ");

const meanOfNumbers = (n1, n2, n3) => {
    let sumNumbers = parseFloat(n1) + parseFloat(n2) + parseFloat(n3);
    let mean = sumNumbers / 3;
    return mean;
}

alert(`Mean of numbers: ${meanOfNumbers(numOne, numTwo, numThree)}`);

// Ejercicio 5

let tempFarenheit = prompt("Type your tempeture on farenheit: ");

const tempToCelsius = (f) => {
    let celsius = (5/9) * (parseFloat(f) -32);
    return Math.floor(celsius);
};

alert(`Temperature on Celsius is: ${tempToCelsius(tempFarenheit)}`);

// Ejercicio 7 
let number = parseFloat(prompt("Type your number: "));

let total = number - (15/100 * number);
alert(`The total is: ${total}`);

// Ejercicio 8
let firstWord = prompt("Type a word");
let secondWord = prompt("Type another word");
let concatWords = firstWord + " " + secondWord;

alert(concatWords);

// Ejercicio 9
let text = prompt("Type a text");
let index = parseInt(prompt("Type a interger to specify an index"));

alert(`The first character in the text is: ${text[0]}`);

if(index <= text.length){
    alert(`The character in that index is ${text[index]}`);
} else {
    alert("Index out of range");
}

// Ejercicio 10
let quantityOfShows = parseInt(prompt("Type the number of shows that you watched: "));

if(quantityOfShows > 3){
    alert(true);
} else {
    alert(false);
}

// Ejercicio 11
// There will be an apropiated solution

let userDate = parseInt(prompt("Type your date: "));

let day = userDate.toString().substring(0, 2);
let month = userDate.toString().substring(2, 4);
let year = userDate.toString().substring(4, 8)

console.log(`${day}/${month}/${year}`);


//Ejercicio 12
const isOddOrEven = (n) => {
    if (n % 2 === 0) {
        return true;
    } else {
        return false;
    }
};

console.log(isOddOrEven(7454));

// Ejercicio 13
let ageCostumer = parseInt(prompt("Type you age: "));
let quantityOfProducts = parseInt(prompt("Total of your products that you buy it: "));

if (ageCostumer <= 18 || quantityOfProducts <= 1) {
    console.log(false);
} else {
    console.log(true);
};

// Ejercicio 14
let textFromUser = "Era mejor de los tiempos, era el peor de los tiempos.";

const isLengthOddOrEven = (s) => {
    if(s.length % 2 === 0){
        return false;
    } else {
        return true;
    }
};

console.log(isLengthOddOrEven(textFromUser));

// Ejercicio 15
let firstWord = prompt("Type a word");
let secondWord = prompt("Type a second word");

const compareSizes = (s1, s2) => {
    if(s1.length < s2.length){
	return true;
    } else {
	return false;
    }
};

alert(compareSizes(firstWord, secondWord));

// Ejercicio 16
// Using charAt() method

let myName = prompt("Type your name");
let anotherName = prompt("Type another name");

if (myName.charAt(0) === anotherName.charAt(0)){
	alert(true);
} else if(myName.charAt(myName.length - 1) === anotherName.charAt(anotherName.length - 1)){
	alert(true);
} else {
	alert(false);
};

// Ejercicio 17

let integer = parseInt(prompt("Type a number"));
let absValue = Math.abs(integer);
alert(`The absolute value of ${integer} is: ${absValue}`);

// Ejercicio 18
let numOne = parseInt(prompt("Type a number"))
let numTwo = parseInt(prompt("Type a second number"))

const greaterNum = (n1, n2) => {
    if(n1 > n2) {
	return `${n1} is greater`;
    } else {
	return `${n2} is greater`;
    };
}

alert(greaterNum(numOne, numTwo));

// Ejercicio 19

let vowels = ["a", "i", "u", "e", "o"];
let letter = prompt("Type a letter")

for(let i = 0; i < vowels.length; i++){
   if(letter === vowels[i]){
	alert("Is vowel");
	break;
   } else {
	alert("Error: data can´t be processed");
	break;
   }
};


// Ejercicio 20
let nOne = parseInt(prompt("Type the first number"));
let nTwo = parseInt(prompt("Type the second number"));
let nThree = parseInt(prompt("Type the thrid number"));

const lessThreeNumbers = (n1, n2, n3) => {
     if(n1 > n2) { 
	    return n2;
     } else if (n2 > n3) {
	    return n3;	
     } else {
	    return n1;
     };
};

alert(lessThreeNumbers(nOne, nTwo, nThree));
