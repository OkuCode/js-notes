function Queue(){
    this.start = 0;
    this.elements = {};
    this.end = 0;
    this.startOut = 0;
    this.elementsOut = {};
    this.endOut = 0;
    
    // Append various elements 
    this.appendElements = (items) => {
        for(let i = this.start; i < items; i++){
            this.elements[this.end] = i;
            this.end += 1
        }
    }

    // Append one element
    this.appendQueue = (item) => {
        this.elements[this.end] = item;
        this.end += 1;
    }

    // Delete selected element
    this.deleteSelectedElement = (item) => {
       if(this.lengthOfQueue() === 0){
           return null;
       } else {
           let dato = this.elements[this.start];
           for(let i = this.start; i < this.end; i++){
                if(this.elements[i] === item){
                    delete this.elements[i];
                    this.start += 1;
                }
           }
           
           for(let j = this.start; j < this.end; j++){
                if(this.elements[i] === undefined){
                    this.elements[i] = this.elements[i + 1];
                    delete this.elements[i + 1]
                }
           }

           return dato;
       }
    }

    this.deleteElement = () => {
        if(this.lengthOfQueue() === 0){
            return null;
        } else {
            const DATA = this.elements[this.start];
            delete this.elements[this.start];
            return DATA;
        }
    }
    
    // Verify the length
    this.lengthOfQueue = () => {
        return this.end - this.start;
    }

    // Verify the length Out
    this.lengthOfQueueOut = () => {
        return this.endOut - this.startOut;
    }


    // Print the elements
    this.printElements = () => {
        if(this.lengthOfQueue() === 0) {
            return null;
        } else {
            let display = '';
            for(let i = this.start; i < this.end; i++){
                display += this.elements[i] + ' ';
            }

            return display;
        }
    }

    this.printElementsOut = () => {
        if(this.lengthOfQueueOut() === 0) {
            return null; 
        } else {
            let displayOut = '';
            for(let i = this.startOut; i < this.endOut; i++){
                displayOut += this.elementsOut[i] + ' ';
            }

            return displayOut;
        }
    }
    
    // Returns an even or odd method
    this.isEvenOrOddQueue = () => {
        if (this.lengthOfQueue() === 0) {
            return `The number is odd: ${this.lengthOfQueue()}`;
        } else {
            const ARRAYEVENODD = [];
            for (let i = this.start; i < this.end; i++){
                (this.elements[i] % 2 === 0) ? ARRAYEVENODD.push(true) : ARRAYEVENODD.push(false);
            }

            return `The values are: ${ARRAYEVENODD}`;
        }
    }

    // Only append even values
    this.appendEvenOrOddValues = () => {
        if(this.lengthOfQueue() === 0) {
            return `The number is the only odd number: ${this.lengthOfQueue()}`;
        } else {
            const evenArray = [];
            const oddArray = [];

            for(let i = this.start; i < this.end; i++) {
                (this.elements[i] % 2 === 0) ? evenArray.push(this.elements[i]) : oddArray.push(this.elements[i]);
            }

            return `Even numbers: [${evenArray}]\nOdd Numbers: [${oddArray}]`;
        }
    }

    // Appending str values
    this.appendStrs = () => {
        if (this.lengthOfQueue() === 0) {
            return null;
        } else {
            const STRARRAY = [];
            for(let i = this.start; i < this.end; i++){
                (typeof(this.elements[i]) === "string") ? STRARRAY.push(this.elements[i]) : this.elements[i];
            }

            return STRARRAY;
        }
    }
    
    // Adding Only numeric numbers
    this.addOnlyNumerics = () => {
        if (this.lengthOfQueue() === 0){
            return `The total of first value is: ${this.lengthOfQueue()}`
        } else {
            let total = 0;
            for(let i = this.start; i < this.end; i++){
                (typeof(this.elements[i]) === "number") ? total += this.elements[i] : this.elements[i]; 
            }

            return `The total of the values is: ${total}`;
        }
    }

    // Verify the type of data 
    this.allTypeDataQueue = () => {
        if (this.lengthOfQueue() === 0){
            return `The total of first value is: ${this.lengthOfQueue()}`
        } else {
            const NUMBERS = [];
            const STRINGS = [];
            const ARRAYS = [];

            for(let i = this.start; i < this.end; i++){
                if(typeof(this.elements[i]) === "number") {
                    NUMBERS.push(this.elements[i]);
                } else if(typeof(this.elements[i]) === "string") {
                    STRINGS.push(this.elements[i]);
                } else {
                    ARRAYS.push(this.elements[i]);
                }
            }

            return `The total of the values is: [${NUMBERS}]\n[${STRINGS}]\n[${ARRAYS}] `;
        }
    }
    
    // Verify if is empty
    this.isEmpty = () => {
        return (this.lengthOfQueue() === 0) ? true : false;
    }

    this.verifyElement = (item) => {
        if(this.lengthOfQueue() === 0){
            return null;
        } else {
            let element;
            for(let i = this.start; i < this.end; i++) {
                if (this.elements[i] === item){
                    element = `Element found: ${this.elements[i]}`
                } else if (this.elements[i] === undefined) {
                    element = "This element is invalid";
                } else {
                    element = "Not found!";
                } 
            }

            return element;
        }
    }
}

const queue = new Queue();
queue.appendElements(5);
queue.deleteSelectedElement(0);
queue.deleteSelectedElement(4);
console.log(queue.printElements());
console.log(queue.printElementsOut());
