class Stack {
    constructor(elements, data) {
        this.elements = elements || [];
        this.data = data;
    }

    // Append one element
    appendElement = (item) => {
        if(item === undefined) {
            return `Insert a valid value`;
        } else {
            return this.elements.push(item);
        }
    }

    // Append various elements
    appendVariousElements = (n) => {
        for(let i = 0; i < n; i++){
            this.elements.push(i);
        }
    };

    // Print the current stack
    printStack = () => {
        return this.elements;
    }

    // Review the length of the stack
    lengthStack = () => {
        return this.elements.length;
    }

    // Verify even or odd
    evenOrOdd = () => {
        let evenOddArray = [];
        for(let i = 0; i < this.lengthStack(); i++ ){
            (i % 2 === 0) ? evenOddArray.push(true) : evenOddArray.push(false);
        }

        return evenOddArray;
    }

    // Add all elements to the stack
    addAllItemsStack = () => {
        let total = 0;

        for(let i  = 0; i < this.lengthStack(); i++){
            total += this.elements[i];
        }

        return total;
    }

    // Filter elements to the stack
    evenAndOddElementsStack = () => {
        let evenElements = [];
        let oddElements = [];
        for (let i = 0; i < this.lengthStack(); i++){
            (i % 2 === 1) ? oddElements.push(i) : evenElements.push(i);
        }

        console.log(evenElements, oddElements);
    }

    // Filter Str elements to the stack
    stringsStack = () => {
        let strElements = [];
        for(let i = 0; i < this.lengthStack(); i++){
            (typeof(this.elements[i]) === "string") ? strElements.push(this.elements[i]) : this.elements[i];
        }

        return strElements;

    }

    // Filter type of elements of the stack
    totalTypeElementsStack = () => {
        let strings = 0;
        let integers = 0;
        let objects = 0;
        let bools = 0;

        for(let i = 0; i < this.lengthStack(); i++){
            if(typeof(this.elements[i]) === "number"){
                integers += 1;
            } else if (typeof(this.elements[i]) === "string"){
                strings += 1;
            } else if(typeof(this.elements[i]) === "boolean"){
                bools += 1;
            } else {
                objects += 1;
            }
        }

        return `Total of elements\nStrings: ${strings}\nIntegers: ${integers}\nArrays: ${objects}\nBooleans: ${bools}`
    }

    // Sort the array
    sortStack = () => {
        return this.elements.sort();
    }

    // Splice, Unshift, Pop, Filter 
    deleteByElement = (element) => {
        //// Loop through the stack and delete the elemet
        if(this.lengthStack() === 0){
            return`There's no elements on the stack`;
        } else {
            for(let i = 0; i < this.lengthStack(); i++){
                if(this.elements[i] === element) {
                    this.data = this.elements.splice(element, 1)
                    //splice(index, count, element)
                }
            }
        }

        // Verify the deleted element
        if(this.data != undefined){
            return `The element has been deleted: ${this.data[0]}`;
        } else {
            return `The element doesn't exists`;
        }
    }
    
    // Delete the item specify the position
    deleteByIndex = (index) => {
        if(this.lengthStack() === 0){
            return `There's no elements on the stack`;
        } else {
            for (let i = 0; i < this.lengthStack(); i++){
                if(index === this.elements.indexOf(this.elements[i])){
                    this.data = array.splice(i, 1)
                }
            }
        }
        
        // Verify the deleted element
        if(this.data != undefined){
            return `The element has been deleted: ${this.data[0]}`
        } else {
            return `The element doesn't exists`
        }
    }

    // Verify it is empty
    isEmpty = () => {
        return (this.lengthStack() === 0) ? true : false;
    }
}


const STACK = new Stack();
STACK.appendElement("Hola");
STACK.appendElement(10);
STACK.appendElement("Helen");
STACK.appendElement(true);

console.log(STACK.printStack());
console.log(STACK.deleteByElement(true));
console.log(STACK.deleteByElement("Helen"))
console.log(STACK.deleteByElement(10))
console.log(STACK.sortStack());
console.log(STACK.printStack());


// Use of objects, sort and reindex, keep in mind for the TEST......

/* Keep in mind

Debo recorrer el arreglo
Por cada elemento verificar si es par o impar
    - Notas:
        - Si el primer elemento es par y el segundo es par - false
        - Si el primer elemento es impar y el segundo es impar - false
        - Si el primer elemeto es par, el segundo impar, pero si hay un elemento consecutivo que es impar - false
        - Si el consecutivo es par despues de un impar - true
        - Si el cualquier elemento del arreglo es un 'String' - false
        
[1,2,3,4] false - 
[2,3,4,5] true
[2,3,4,5,6,7,8,9] true 
[3,5,1,4] false
["sd",3,4,3] false

*/
