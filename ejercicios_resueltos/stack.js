class Stack {
    constructor(elements) {
        this.elements = elements || [];
    }

    appendElement = (item) => {
        if(item === undefined){
            return `Insert a valid value`;
        } else {
            return this.elements.push(item);
        }
    }

    lengthStack = () => {
        return this.elements.length;
    }

    removeElementAtLast = () => {
        const DATA = this.elements[this.elements - 1];
        this.elements.pop()
        return `The element has been deleted`;
    }

    removeElementAtBegin = () => {
        const DATA = this.elements[this.elements - 1];
        this.elements.shift();
        return `The element has been deleted`
    }
    
    isEmpty = () => {
        return (this.elements.length === 0) ? true : false;
    }

    printStack = () => {
        return this.elements;
    }
}

const STACK = new Stack();
// Friends name, ERROR I HAVE NOT FRIENDS
STACK.appendElement("Mario");
STACK.appendElement("Peach");
STACK.appendElement("Rosalina");
STACK.appendElement("Daisy");
STACK.appendElement("Luigi");

// length stack and print current stack
console.log(STACK.lengthStack());
console.log(STACK.printStack());

// Remove two elements and the beggining and one at the end
console.log(STACK.removeElementAtBegin());
console.log(STACK.removeElementAtBegin());
console.log(STACK.removeElementAtLast());

// length stack and print current stack
console.log(STACK.lengthStack());
console.log(STACK.printStack());

// Append family members
STACK.appendElement("Maria");
STACK.appendElement("Francy");
STACK.appendElement("Hector");
STACK.appendElement("Jose");
STACK.appendElement("Ancizar");

// length stack and print current stack
console.log(STACK.lengthStack());
console.log(STACK.printStack());


// Delete all elements
for(let i = 0; i < STACK.lengthStack(); i++){
    STACK.removeElementAtBegin();
    STACK.removeElementAtLast();
}

STACK.removeElementAtLast();

// length stack and print current stack
console.log(STACK.lengthStack());
console.log(STACK.printStack());

console.log(STACK.isEmpty());