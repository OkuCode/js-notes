/*
1. Crear un Fila con 5 elementos que contengan cada un nombre de un amigo
2. Mostrar la longitud de la Fila
3. Desencolar los 3 primeros amigos
4. Agregar 5 nombres de tu familia
5. revisar si esta vacia
6. imprimir toda la fila
7. ver el siguiente
8. desencolar hasta estar vacia
9. revisar si esta vacia 
*/

function Queue() {
    this.elements = {};
    this.end = 0;
    this.start = 0;

    this.popQueue = () => {
        if (this.end === this.start){
            return null;
        } else {
            const DATA = this.elements[this.start];
            delete this.elements[this.start];
            this.start +=1;
            return DATA; 
        }
    }

    this.appendQueue = (data) => {
        this.elements[this.end] = data;
        this.end += 1;
    }

    this.lengthQueue = () => {
        return this.end - this.start;
    }

    this.isQueueEmpty = () => {
        return (this.lengthQueue() === 0) ? 'Si' : 'No';
    }

    this.nextElement = () => {
        if(this.lengthQueue() === 0) {
            return null;
        } else {
            return this.elements[this.start];
        }
    }

    this.printQueue = () => {
        if (this.lengthQueue() === 0) {
            return null;
        } else {
            let display = '';
            for(let i = this.start; i < this.end; i++){
                display += ` ${this.elements[i]} `;
            }

            return display;
        }
    }
}

const queueDudes = new Queue();
 // Appending data
queueDudes.appendQueue("Diego");
queueDudes.appendQueue("Michelle");
queueDudes.appendQueue("Mauricio");
queueDudes.appendQueue("Jhon");
queueDudes.appendQueue("Johan");


 // Deleting data 
queueDudes.popQueue();
queueDudes.popQueue();
queueDudes.popQueue();

// Appending familiy data
queueDudes.appendQueue("Maria");
queueDudes.appendQueue("Francy");
queueDudes.appendQueue("Jose");
queueDudes.appendQueue("Alejandro");
queueDudes.appendQueue("Hector"); 

// Deleting data again
queueDudes.popQueue();
queueDudes.popQueue();
queueDudes.popQueue();
queueDudes.popQueue();
queueDudes.popQueue();
queueDudes.popQueue();
queueDudes.popQueue();


console.log("Datos actuales: " + queueDudes.printQueue());
console.log("Siguiente elemento: " + queueDudes.nextElement());
console.log("Largo de la fila: " + queueDudes.lengthQueue());
console.log("Esta vacio: " + queueDudes.isQueueEmpty())