// Person Class
class Person {
    constructor(name, age){
        this.name = name;
        this.age = age;
        this.price;
    }

    //Asign price to the respective age
    assignPrice(){
        if(this.age < 0){
            return `Please insert a valid age, Error`
        } else if (this.age > 5 && this.age < 10){
            this.price = 10000;
        } else if (this.age > 10 && this.age < 17){
            this.price = 15000;
        } else {
            this.price = 30000;
        }
    }

    // Print the object
    strPerson(){
        return `Name: ${this.name}\nAge: ${this.age}\nPrice: ${this.price}`;
    }
}


class Node {
    constructor(data, next){
        this.data = data;
        this.next = next;
    };
};

class LinkedList {
    constructor(){
        this.head = null;
        this.size = 0;
    };

    add(data){
        const NEWNODE = new Node(data, null);
        if(this.head != null){
            let current = this.head;
            
            while(current.next){
                current = current.next;
            };
            
            current.next = NEWNODE;

        } else {
            this.head = NEWNODE;
        };

        this.size += 1;
    };

    addElements(n){
        for(let i = 0; i < n; i++){
            const NEWNODE = new Node(i, null)
            if (this.head != null){
                let current = this.head;
                while(current.next){
                    current = current.next;
                };

                current.next = NEWNODE;
            } else {
                this.head = NEWNODE;
            };

            this.size += 1;
        };
    };

    print(){
        if(this.head != null){
            let current = this.head;
            let display = "";

            while(current){
                if(typeof(current.data) === "string"){
                    display += current.data += " -> ";
                } else if(typeof(current.data) === "object") {
                    display += `${JSON.stringify(current.data)} -> `;
                } else {
                    display += current.data += " -> ";
                }

                current = current.next;
            };

            display += "null"
            return display;

        } else {
            return `There's no data available`;
        };
    };

    length() {
        if(this.head != null){
            let count = 1;
            let current = this.head;
            while(current.next){
                count += 1;
                current = current.next;
            };

            return `Total of elements: ${count}`;

        } else {
            return `There's no elements on the list`;
        };
    };
    
    // Finish this method
    evenOrOdd(){
        if(this.head != null){
            // Verify the data inside of the head
            let current = this.head;
            if(current.data % 2 != 0){
                return false;
            } else if(typeof(current.data) == "string"){
                return false;
            } else {
                return true;
            }
        } else {
            return `There's no data available`;
        }
    };

    // Odd and even lists, only for numbers
    orderingEvenOdd(){
        let evenList = new LinkedList();
        let oddList = new LinkedList();
        
        if(this.head != null){
            let current = this.head;
            let displayEven = "";
            let displayOdd = "";
            while(current){
                if(current.data % 2 === 0){
                    evenList.add(current.data);
                    displayEven += `${current.data} -> `;
                } else {
                    oddList.add(current.data);
                    displayOdd += `${current.data} -> `; 
                } current = current.next
            } return `Even elements: ${displayEven += "null"}\nOdd Elements: ${displayOdd += "null"}`; 
        } else {
            return `There's no data on the list`
        };
    };

    //Only Strings
    orderingStrings(){
        let strList = new LinkedList();
        let displayStr = "";
        if(this.head != null){
            let current = this.head;
            while(current){
                if(typeof(current.data) === 'string'){
                    strList.add(current.data);
                    displayStr += `${current.data} -> `
                } 
                current = current.next;
            } return `Str Elements: ${displayStr += 'null'}`;
        } else {
            return `There's no data on the list`;
        };
    };

    // Delete by element
    deleteByIndex(index){
        if(index < 0 || index > this.size){
            return `Invalid Index, Position failed`;
        } else {
            let current = this.head;
            let previous;

            if(index === 0){
                this.head = current.next;
            } else {
                for(let i = 0; i < index; i++){
                    previous = current;
                    current = current.next;
                };

                previous.next = current.next;
            };

            this.size -= 1;
            return current.data;
        };
    };

    // Ordering by typeof
    totalElemByType(){
        let strs = 0;
        let numbers = 0;
        let objects = 0;

        if(this.head != null){
            let current = this.head;
            while(current){
                if(typeof(current.data) === 'number'){
                    numbers += 1
                } else if(typeof(current.data) === 'string'){
                    strs += 1;
                } else {
                    objects += 1;
                }

                current = current.next;
            }

            return `Total\nStrings: ${strs}\nNumbers: ${numbers}\nObjects: ${objects}`

        } else {
            return `There's no data on the list`;
        };
    };
    
    // Delete element by data
    deleteByElement(data){
        if(this.head != null){
            let current = this.head;
            if(current.data === data){
                this.head = current.next;
            } else {
               let index = 0;
               while(current){
                   if(current.data === data){
                       return this.deleteByIndex(index)
                   }
                   current = current.next;
                   index += 1;
               }
            };
        } else {
            return "There's no data on the list";
        };
    };

    // Total of sold tickets
    totalPrices(){
        if(this.head != null){
            let current = this.head;
            let total = 0;
            while(current){
                total += current.data["price"];
                current = current.next
            }

            return `The total of all sold tickets are: ${total}`;
        }
    };
};

// Person one
const person = new Person("Alice", 9);
person.assignPrice();

// Person three
const person_two = new Person("Remilia", 18);
person_two.assignPrice();

// Person four
const person_three = new Person("Reisen", 15);
person_three.assignPrice();

// Creating the linked list 
const linkedlist = new LinkedList();

// Adding the persons
linkedlist.add(person);
linkedlist.add(person_two);
linkedlist.add(person_three);

// Printing the total of tickets
console.log(linkedlist.totalPrices());

// Printing the list
linkedlist.print();
