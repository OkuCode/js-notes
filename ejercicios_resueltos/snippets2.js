// Ejercicios parte 2
// Ejercicio 1 (6)
let number = parseInt(prompt("Type a number"));

if(number % 5 === 0) {
    console.log("It's divisible");
} else {
    console.log("Not divisible");
};

// Ejercicio 2 (9)
let numOne = parseInt(prompt("Type a number"));
let numTwo = parseInt(prompt("Type a another number"));

const greaterNumber = (n1, n2) => {
    return (n1 < n2) ? n2 : n1;
};

alert(greaterNumber(numOne, numTwo));

//Ejercicio 3 (18)
let number = 10;
let arrayNumbers = [];

for(let i = 0; i <= number; i++){
    arrayNumbers.push(i + 1);
}

console.log(arrayNumbers);

// Ejercicio 4 (24) 
let number = 5;
let total = 0;

for(let i = 0; i <= number; i++){
    total += i;
}

console.log(total);

// Ejercicio 5 (37)
let rows = 5;
let str = "";

for (let i = 0; i < rows; i++){
    for (let j = 0; j < rows - i; j++){
        str +="#";
    } 
    str += "\n";
}

console.log(str)

// Ejercicio 6 (39)
const myFunction = (any) => {
     return any;
}

console.log(myFunction(5));

// Ejercicio 7 (40)
// The function needs the return keyword, by default all funtions return *undefined*
function multiply(a, b){
     return a * b;
};

console.log(multiply(10, 20));

// Ejercicio 8 (47)
let numberArray = [1, 2, 3, 4, 5];
let newArray = [];

for(let i = 0; i < numberArray.length; i++){
	newArray.push(numberArray[i] + 1);
};

console.log(newArray);

// Ejercicio 9 (52)
let color = prompt("Type a color");
let colorArray = ["azul", "amarillo", "rojo", "rosa", "verde", "cafe"];

for(let i = 0; i < colorArray.length; i++){
    if(color === colorArray[i]){
	console.log(true);
    } else {
	console.log(false);
    }
    
    // Using ternary operator
    // (color === colorArray[i] ? true : false)
}

// Ejercicio 10 (53)

let userStr = prompt("Type numbers separate by coma");
let array = userStr.split(",");
alert(`[${array}]`)

