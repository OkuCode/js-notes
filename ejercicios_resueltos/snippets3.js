// Classes Exercises

// Ejercicio 1 y 1.1 -----------------------------------------------------------------

class Persona {
    constructor(nombre, edad, cedula) {
        this.nombre = nombre;
        this.edad = edad;
        this.cedula = cedula;
    }

    mostrarInfo(){
        return `Nombre: ${this.nombre}, Edad: ${this.edad}, Cedula: ${this.cedula}`;
    }

    esMayorDeEdad(){
        return (this.edad >= 18) ? "Es mayor de edad" : "No es mayor de edad";  
    }
}

// Ejercicio 2 y 2.1-2.2-2.3 ------------------------------------------------------

class Cuenta {
    constructor (titular, cantidad) {
        this.titular = titular
        this.cantidad = cantidad;
    }

    mostrarInfo(){
        return `Titular: ${this.titular}, Cantidad: ${this.cantidad}`;
    }

    ingresarCantidad(cantidad){
        if (cantidad <= 0 || cantidad === undefined) {
            return "Monto invalido, especifique el monto que desea ingresar";
        } else {
            this.cantidad += cantidad;
            return `La cantidad es ${cantidad}`;
        }
    }

    retirar(cantidad){
        if (cantidad >= this.cantidad){
            return `Tu cuenta está en números rojos, solo tienes ${this.cantidad}`
        } else {
            this.cantidad -= cantidad;
            return `Total cuenta: ${this.cantidad}`;
        }
    }
}

/*const account = new Cuenta("Sakuya Izayoi", 130000);
console.log(account.mostrarInfo())
console.log(account.ingresarCantidad(600000))
console.log(account.mostrarInfo())
console.log(account.retirar(729500))
*/

// Ejercicio 3 ----------------------------------------------------------------------------

class Formulas {
    constructor(value){
        this.value = value;
    }

    sumarValores(newValue){
        return this.value + newValue;
    }
    
    // Cambiar el valor y la condicion del return 
    fibonacci(quantity){
        let numOne = 0;
        let numTwo = 1;
        let sum;

        for (let i = 0; i < quantity; i++){
            sum = numOne + numTwo;
            numOne = numTwo;
            numTwo = sum;
        } 

        return numTwo;
    }

    module(){
        let zeroModule = [];
        let nonZeroModule = [];

        for(let i = 0; i < this.value; i++){
            if (i % 2 === 0){
                zeroModule.push(i);
            } else {
                nonZeroModule.push(i);
            }
        }

        return `[${zeroModule}]\n[${nonZeroModule}]`;
    }

    esPrimo() {
        for (let i = 2; i < this.value; i++){
            if(this.value % i === 0){
                return false;
            }
        } return n != 0;
    }

    numPrimos(cantidad){
        for(let i = 0; i < cantidad; i++) {
            if(this.esPrimo(i)){
                console.log(i);
            }
        }       
    }
}

//Ejercicio 4 ------------------------------------------------------------------------------------

class Person {
    constructor(nombre, edad, DNI, genero, peso, altura){
        this.nombre = nombre;
        this.edad = edad;
        this.DNI = DNI;
        this.genero = genero;
        this.peso = peso;
        this.altura = altura;

    };

    calcularIMC(){
        const FX = (this.peso/(Math.pow(this.altura, 2)));
        if(FX <= 20){
            return -1;
        } else if (FX > 20 && FX <= 25){
            return 0;
        } else {
            return 1;
        };
    };

    isOlder(){
        return (this.edad <= 18) ? false : true;  
    };

    comprobarGenero(){
        if(this.genero[0].toLowerCase() === "f"){
            return "Femenino";
        } else if (this.genero[0].toLowerCase() === "m") {
            return "Masculino";
        } else {
            return `Error, binary genere: ${this.genero[0]}`
        }
    }
}

// Ejercicio 5 -----------------------------------------------------------------

// Se debe de hacer con le password ingresado no con el largo del password debo revisar el
// Enunciado

class Password {
    constructor(lengthPass, password){
        this.lengthPass = lengthPass;
        this.password = password;
    };

    generateRandomPasswd(){
        this.randPasswd = '';
        this.alphanumericChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        
        for (let i = 0; i <= this.lengthPass; i++){
            this.randPasswd += this.alphanumericChars.charAt(Math.random() * this.alphanumericChars.length);
        }

        return this.randPasswd;
    }

    isStrong(){
        this.upperLetters = 0;
        this.lowerLetters = 0;
        this.numbers = 0;
        this.specialChars = 0;
        
        for(let i = 0; i < this.password.length; i++){
            if(this.password.charCodeAt(i) >= 65 && this.password.charCodeAt(i) <= 90){
                this.upperLetters += 1;
           } else if (this.password.charCodeAt(i) >= 97 && this.password.charCodeAt(i) <= 127) {
                this.lowerLetters += 1;
           } else if (this.password.charCodeAt(i) >= 48 && this.password.charCodeAt(i) <= 57){
               this.numbers += 1;
           } else {
               this.specialChars += 1;
           }
       }

       return (this.upperLetters >= 2 && this.lowerLetters >= 1 && this.numbers >= 5) ? true : false;
    }


    // Verify password it is secure
    securityPasswd(){
        this.totalChars = 0;
        for (let i = 0; i < this.password.length; i++){
           if(this.password.charCodeAt(i) >= 65 && this.password.charCodeAt(i) <= 90){
                this.totalChars += 1;
           } else if (this.password.charCodeAt(i) >= 97 && this.password.charCodeAt(i) <= 127) {
                this.totalChars += 1;
           } else if (this.password.charCodeAt(i) >= 48 && this.password.charCodeAt(i) <= 57){
               this.totalChars += 1;
           } else {
               this.totalChars += 1;
           }
        }

        if(this.totalChars >= 1 && this.totalChars <= 6){
            return `Weak password, it has: ${this.totalChars}`;
        } else if(this.totalChars >= 7 && this.totalChars <= 10){
            return `Medium password, it has: ${this.totalChars}`;
        } else {
            return `Strong password, it has: ${this.totalChars}`;
        }
    }    
}

let passwd = new Password(12, "PHA7Vqj7aS1B745678")
console.log(passwd.isStrong());
console.log(passwd.securityPasswd());
console.log(passwd.generateRandomPasswd())


//Ejercicio 6 ------------------------------------------------------------------------

class Counter {
    constructor (value){
        this.value = value;
    }

    reset(){
        this.command = "Reset";
        return this.value = 0;
    }

    increment(){
        this.command = "Increment";
        return this.value += 1;
    }

    decrement(){
        this.command = "Decrement";
        return this.value -= 1;
    }

    actualValue(newValue){
        this.command = "Actual value";
        this.value = newValue;
        return newValue;
    }

    // Ejercicio 7
    commandLog(){
        return (this.command === undefined) ? "Error, command not found" : this.command; 
    }
}

// -----------------------------------------------------------------------------

// Ejercicio 7, 8
// The output in joule km . m2/ s2
// Cuando come algo de comida su energia aumenta adquiere 4 joules por cada gramo
// Para volar:
// Si necesita despegar gasta 10 joules aplica para el aterrizaje, durate el vuelo
// Necesita consumir por 1 joule por cada kilometro recorrido
// Falta tener control del valor de la energia al cual se le esta restando

function Elma() {
    this.energy;

    this.remainingEnergy = () => {
        if (this.energy <= 0){
            return "The Dragon hasn't energy right now"
        } return `Elma has: ${this.energy} joules of energy`;
    };


    this.hasEaten = (g) => {
        return (this.energy === undefined) ?  
            this.energy = Math.floor((g / 1000) * 4 / 0.001) : this.energy += Math.floor((g / 1000) * 4 / 0.001);
    };


    this.hasFlew = (km) => {
        this.energy = this.energy - km;
        return this.energy;
    };

    this.hasLanded = (km) => {
        this.energy = this.energy - km;
        return this.energy;
    };

    this.animicState = () => {
        if(this.energy <= 50) {
            return `She is weak, ${this.remainingEnergy()}`;
        } else if (this.energy >= 50 && this.energy <= 500) {
            return `She is normal, ${this.remainingEnergy()}`;
        } else if (this.energy >= 500 && this.energy <= 1000) {
            return `She is happy, ${this.remainingEnergy()} !!!`;
        } else {
            return `She is out of control, ${this.remainingEnergy()}`
        };
    }

    this.sheWantsToFly = () => {

        this.flew = (this.energy / 5);

        if(this.energy >= 300 && this.energy <= 400 && this.energy % 20 === 0){
            this.flew += 25;
            return `She wants to fly: ${this.flew} Km`;
        } else {
            return `She wants to fly : ${this.flew} Km`;
        };
    }
};


// Ejercicio 10

function Calc(value){
    this.value = value;

    this.loadNumber = () => {
        if(this.value != undefined){
            return this.value;   
        } else {
            return `Error, Can't read the value: ${this.value}`
        };
    };

    this.addNumber = (number) => {
        this.result;
        return this.result = this.value += number;
    };

    this.substractNumber = (number) => {
       return this.result -= number; 
    };

    this.multiplyNumber = (number) => {
        return this.result *= number;
    };

    this.currentValue = () => {
        if (this.result != undefined) {
            return `Current value: ${this.result}`
        } else if (this.result === 0) {
            return `Current value: ${this.result}`
        } else {
            return this.loadNumber();
        }
    };
}

//let calculator = new Calc(10);
//calculator.loadNumber()
//calculator.addNumber(4)
//console.log(calculator.currentValue());

// Ejercicio 11 -------------------------------------------------

class Book {
    constructor(title, author, ownedBooks, borrowedBooks){
        this.title = title;
        this.author = author;
        this.ownedBooks = ownedBooks;
        this.borrowedBooks = borrowedBooks;
    }

    wasBorrowed(){
        if(this.ownedBooks > 0){
            this.ownedBooks -= 1;
            this.borrowedBooks += 1;
            return this.ownedBooks;
        } else {
            return this.ownedBooks;
        }
    };

    wasRetrived(){
        if(this.borrowedBooks > 0){
            this.ownedBooks += 1;
            this.borrowedBooks -= 1;
            return this.borrowedBooks;
        } else {
            return this.borrowedBooks;
        }
    }
}

Book.prototype.toString = function bookRepr() {
    return `Book{ \n\ttitle: ${this.title}, \n\tauthor: ${this.author}, \n\townedBooks: ${this.ownedBooks}, \n\tborrowedBooks: ${this.borrowedBooks} \n}`;
}

//const book = new Book("Eloquent Javascript", "Marijn Haverbeke", 3, 0);
//book.wasBorrowed();
//book.wasBorrowed();
//book.wasBorrowed();
//book.wasRetrived();
//book.wasRetrived();
//console.log(book.toString())

// Ejercicio 12 -------------------------------------------------
// What is needed to be fixed
// When you find a atomic pile and the value is greater than 100, needs to be 100
// DefensiveStrength ?, No tengo idea

class Starship {
    constructor(energy, armor) {
        this.energy = energy || 50;
        this.armor = armor || 5;
     }

    foundAtomicPile(){
        const ATOMICPILE = 25;
        if (this.energy != 100){
            return this.energy += ATOMICPILE;
        } else {
            return this.energy = 100;
        }
    };

    foundShieldArmor() {
        const SHIELDARMOR = 10;
        if(this.armor >= 0 && this.armor <= 20){
           return this.armor += SHIELDARMOR; 
        } else {
            return this.armor = 20;
        };
    };

   attackComming(atk) {
       if (this.armor >= atk) {
            return this.armor -= atk;
       } else {
           return this.energy -= atk; 
       }
   }

    // Ejercicio 13
    needsArmor() {
        if(this.armor === 0 && this.energy <= 20) {
            return true;
        } else {
            return false;
        }
    }

    ofensiveAttack() {
        const OFFENSIVEFX = (this.energy - 20) / 2;

        if(this.energy <= 20) {
            return `Attack strength points: ${OFFENSIVEFX}`;
        } else {
            return `Attack strength points: ${OFFENSIVEFX}`;
        }
    }
}

//const enterprise = new Starship();
////enterprise.foundShieldArmor();
////enterprise.foundShieldArmor();
////enterprise.foundShieldArmor();

////enterprise.foundAtomicPile();
////enterprise.foundAtomicPile();
////enterprise.foundAtomicPile();

////// Let's attack the starship
////enterprise.attackComming(20)
////enterprise.attackComming(20)

////enterprise.foundShieldArmor();
////enterprise.foundShieldArmor();

////enterprise.attackComming(20)
////enterprise.attackComming(20)

////enterprise.foundAtomicPile();
////enterprise.foundAtomicPile();
//console.log(enterprise.ofensiveAttack())
//console.log(enterprise)

// Ejercicio 14
//Para eso, nos piden hacer un metodo constructor concentrado en las características del motor. El prototipo de motor tiene 5 cambios (de primera a quinta), y soporta hasta 5000 RPM -> Para nuestro caso el motor cambiara de rpms a 900
// 1 = 500;
// 2 = 1400;
// 3 = 2300;
// 4 = 3200;
// 5 = 4100 a 5000 -> Que es el maximo de rpm que llegara
//
// Verificar al momento de hacer el aumento de rpms

function MotorSpec() {
    const CAMBIODEMARCHA = 900;
    this.rpm;
    this.marcha;

    this.startEngine = () => {
        this.marcha = 1;
        this.rpm = 500;
        return `The engine has started: ${this.rpm}`
    };

    this.up = () => {
        this.marcha += 1;
        this.rpm += CAMBIODEMARCHA;
        return this.rpm;
    };

    this.down = () => {
        this.marcha -= 1;
        this.rpm -= CAMBIODEMARCHA;
        return this.rpm;
    }

    this.upRpms = (quantity) => {
        return this.rpm = this.rpm += quantity;
    }

    this.downRpms = (quantity) => {
        return this.rpm = this.rpm -= quantity;
    }


    this.checkRmpsAndCambio = () => {
        if(this.rpm >= 500 && this.rpm <= 1300) {
            return `Engine has: ${this.rpm} rpms, Cambio: ${this.marcha}`;
        } else if (this.rpm >= 1400 && this.rpm <= 2200) {
            return `Engine has: ${this.rpm} rpms, Cambio: ${this.marcha}`;
        } else if (this.rpm >= 2300 && this.rpm <= 3100) {
            return `Engine has: ${this.rpm} rpms, Cambio: ${this.marcha}`;
        } else if (this.rpm >= 3200 && this.rpm <= 4000) {
            return `Engine has: ${this.rpm} rpms, Cambio: ${this.marcha}`;
        } else if (this.rpm >= 4100 && this.rpm <= 5000) {
            return `Engine has: ${this.rpm} rpms, Cambio: ${this.marcha}`;
        } else {
            return `The engine is exceed the max rmp value, WARNING!!!: ${this.rpm} rpms`;
        }
    }

    this.speedCalc = () => {
        if(this.rpm <= 5000){
            return `Velocity is: ${Math.ceil((this.rpm / 100) * (this.marcha + (this.marcha / 2)))} km/h`;
        } else {
            return `You exceed the rpm value: ${this.rpm}`;
        }
    }
}

const engine = new MotorSpec();
console.log(engine.startEngine());
engine.upRpms(3000)
console.log(engine.checkRmpsAndCambio());
console.log(engine.speedCalc())
